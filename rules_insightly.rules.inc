<?php

/**
 * @file
 * Rules integration for the insightly module.
 */

/**
 * Implementation of hook_rules_action_info()
 */
function rules_insightly_rules_action_info() {
  $actions = array();

  // Add a contact on insightly
  $actions['rules_insightly_action_add_contact'] = array(
    'label' => t('Add Insightly Contact'),
    'group' => t('Rules Insightly'),
    'parameter' => array(
      'user' => array(
        'type' => 'user',
        'label' => t('User'),
      ),
      'fields' => array(
        'type' => 'text',
        'label' => t('Contact Default Fields'),
        'description' => t('Format:FIELD_NAME|VALUE; (Important: no spaces near "|")'),
        'optional' => TRUE,
      ),
      'custom_fields' => array(
        'type' => 'text',
        'label' => t('Contact Custom Fields'),
        'description' => t('Format: CUSTOM_FIELD_ID|CUSTOM_FIELD_VALUE; (Important: no spaces near "|")'),
        'optional' => TRUE,
      ),
      'contactinfos' => array(
        'type' => 'text',
        'label' => t('Contact Infos'),
        'description' => t('Format: CONTACT_INFO_TYPE (EMAIL,WEBSITE,...)|CONTACT_INFO_VALUE; (Important: no spaces near "|")'),
        'optional' => TRUE,
      ),
      'addresses' => array(
        'type' => 'text',
        'label' => t('Adresses'),
        'description' => t('Format: ADDRESS_TYPE (WORK,HOME,...)|{STREET|VALUE,CITY|VALUE,...}; (Important: no spaces near "|")'),
        'optional' => TRUE,
      ),
      'tags' => array(
        'type' => 'text',
        'label' => t('Contact Tags'),
        'description' => t('Format: TAG_NAME|TAG_VALUE;'),
        'optional' => TRUE,
      ),
    ),
  );
  // Update a contact on insightly
  $actions['rules_insightly_action_update_contact'] = array(
    'label' => t('Update Insightly Contact'),
    'group' => t('Rules Insightly'),
    'parameter' => array(
      'user' => array(
        'type' => 'user',
        'label' => t('User'),
      ),
      'fields' => array(
        'type' => 'text',
        'label' => t('Contact Default Fields'),
        'description' => t('Format:FIELD_NAME|VALUE; (Important: no spaces near "|")'),
        'optional' => TRUE,
      ),
      'custom_fields' => array(
        'type' => 'text',
        'label' => t('Contact Custom Fields'),
        'description' => t('Format: CUSTOM_FIELD_ID|CUSTOM_FIELD_VALUE; (Important: no spaces near "|")'),
        'optional' => TRUE,
      ),
      'contactinfos' => array(
        'type' => 'text',
        'label' => t('Contact Infos'),
        'description' => t('Format: CONTACT_INFO_TYPE (EMAIL,WEBSITE,...)|CONTACT_INFO_VALUE; (Important: no spaces near "|")'),
        'optional' => TRUE,
      ),
      'addresses' => array(
        'type' => 'text',
        'label' => t('Adresses'),
        'description' => t('Format: ADDRESS_TYPE (WORK,HOME,...)|{STREET|VALUE,CITY|VALUE,...}; (Important: no spaces near "|")'),
        'optional' => TRUE,
      ),
      'tags' => array(
        'type' => 'text',
        'label' => t('Contact Tags'),
        'description' => t('Format: TAG_NAME|TAG_VALUE'),
        'optional' => TRUE,
      ),
    ),
  );
  // Add/Update a opportunity on insightly
  $actions['rules_insightly_action_add_update_opportunity'] = array(
    'label' => t('Add/Update Insightly Opportunity'),
    'group' => t('Rules Insightly'),
    'parameter' => array(
      'fields' => array(
        'type' => 'text',
        'label' => t('Opporunity Default Fields'),
        'description' => t('Format:FIELD_NAME|VALUE; (Important: no spaces near "|")'),
      ),
      'custom_fields' => array(
        'type' => 'text',
        'label' => t('Opporunity Custom Fields'),
        'description' => t('Format: CUSTOM_FIELD_ID|CUSTOM_FIELD_VALUE; (Important: no spaces near "|")'),
        'optional' => TRUE,
      ),
      'tags' => array(
        'type' => 'text',
        'label' => t('Opporunity Tags'),
        'description' => t('Format: TAG_NAME|TAG_VALUE'),
        'optional' => TRUE,
      ),
    ),
  );
  // Add an organization on insightly
  $actions['rules_insightly_action_add_organization'] = array(
    'label' => t('Add Insightly Organisation'),
    'group' => t('Rules Insightly'),
    'parameter' => array(
      'fields' => array(
        'type' => 'text',
        'label' => t('Organisation Default Fields'),
        'description' => t('Format:FIELD_NAME|VALUE; (Important: no spaces near "|")'),
      ),
      'custom_fields' => array(
        'type' => 'text',
        'label' => t('Organisation Custom Fields'),
        'description' => t('Format: CUSTOM_FIELD_ID|CUSTOM_FIELD_VALUE; (Important: no spaces near "|")'),
        'optional' => TRUE,
      ),
      'contactinfos' => array(
        'type' => 'text',
        'label' => t('Contact Infos'),
        'description' => t('Format: CONTACT_INFO_TYPE (EMAIL,WEBSITE,...)|CONTACT_INFO_VALUE; (Important: no spaces near "|")'),
        'optional' => TRUE,
      ),
      'addresses' => array(
        'type' => 'text',
        'label' => t('Adresses'),
        'description' => t('Format: ADDRESS_TYPE (WORK,HOME,...)|{STREET|VALUE,CITY|VALUE,...}; (Important: no spaces near "|")'),
        'optional' => TRUE,
      ),
      'tags' => array(
        'type' => 'text',
        'label' => t('Contact Tags'),
        'description' => t('Format: TAG_NAME|TAG_VALUE;'),
        'optional' => TRUE,
      ),
    ),
  );
  // Update an Organisation on insightly
  $actions['rules_insightly_action_update_organization'] = array(
    'label' => t('Update Insightly Organisation'),
    'group' => t('Rules Insightly'),
    'parameter' => array(
      'organization_name' => array(
        'type' => 'text',
        'label' => t('Organisation Name'),
      ),
      'fields' => array(
        'type' => 'text',
        'label' => t('Organisation Default Fields'),
        'description' => t('Format:FIELD_NAME|VALUE; (Important: no spaces near "|")'),
        'optional' => TRUE,
      ),
      'custom_fields' => array(
        'type' => 'text',
        'label' => t('Organisation Custom Fields'),
        'description' => t('Format: CUSTOM_FIELD_ID|CUSTOM_FIELD_VALUE; (Important: no spaces near "|")'),
        'optional' => TRUE,
      ),
      'contactinfos' => array(
        'type' => 'text',
        'label' => t('Contact Infos'),
        'description' => t('Format: CONTACT_INFO_TYPE (EMAIL,WEBSITE,...)|CONTACT_INFO_VALUE; (Important: no spaces near "|")'),
        'optional' => TRUE,
      ),
      'addresses' => array(
        'type' => 'text',
        'label' => t('Adresses'),
        'description' => t('Format: ADDRESS_TYPE (WORK,HOME,...)|{STREET|VALUE,CITY|VALUE,...}; (Important: no spaces near "|")'),
        'optional' => TRUE,
      ),
      'tags' => array(
        'type' => 'text',
        'label' => t('Contact Tags'),
        'description' => t('Format: TAG_NAME|TAG_VALUE'),
        'optional' => TRUE,
      ),
    ),
  );
  return $actions;
}

/**
 * Action callback: Add contact on insightly
 */
function rules_insightly_action_add_contact($user, $fields, $custom_fields, $contactinfos,$addresses, $tags) {
  $contact = (object) array();
  _rules_insightly_create_default_fields($fields, $contact);

  // Set the contact's first name in case it's not provided in the default fields
  if (!isset($contact->FIRST_NAME)) {
    $contact->FIRST_NAME = $user->field_full_name['und'][0]['given'];
  }
  // Set the contact's last name in case it's not provided in the default fields
  if (!isset($contact->LAST_NAME)) {
    $contact->LAST_NAME = $user->field_full_name['und'][0]['family'];
  }
  _rules_insightly_create_custom_fields($custom_fields, $contact);
  _rules_insightly_create_contact_info($contactinfos, $contact);
  _rules_insightly_create_addresses($addresses, $contact);
  _rules_insightly_create_tags($tags, $contact);
  $args = func_get_args();
  drupal_alter('rules_insightly_action_add_contact', $contact, $args);
  $i = rules_insightly_get_object();
  try {
    $add = $i->addContact($contact);
  } catch (Exception $e) {
    watchdog('rules_insightly_action_add_contact', $e->getMessage(), WATCHDOG_ERROR);
  }
  return $add;
}

/**
 * Action callback: Update contact on insightly
 */
function rules_insightly_action_update_contact($user, $fields, $custom_fields, $contactinfos,$addresses, $tags) {
  $options['email'] = $user->mail;
  $emails = multiple_email_load_addresses($user->uid);
  $i = rules_insightly_get_object();
  $contacts = $i->getContacts($options);
  $contact = reset($contacts);
  foreach ($emails[0] as $email) {
    if (!_rules_insightly_contact_email_exist($email, $contact)) {
      $contact->CONTACTINFOS[] = _rules_insightly_contact_email($email);
    }
  }
  _rules_insightly_create_default_fields($fields, $contact);
  _rules_insightly_create_custom_fields($custom_fields, $contact);
  _rules_insightly_create_contact_info($contactinfos, $contact);
  _rules_insightly_create_addresses($addresses, $contact);
  _rules_insightly_create_tags($tags, $contact);
  $args = func_get_args();
  drupal_alter('rules_insightly_action_update_contact', $contact, $args);
  try {
    $update = $i->addContact($contact);
  } catch (Exception $e) {
    watchdog('rules_insightly_action_update_contact', $e->getMessage(), WATCHDOG_ERROR);
  }
  return $update;
}

/**
 * Action callback: Add/Update opportunity on insightly
 */
function rules_insightly_action_add_update_opportunity($fields, $custom_fields, $tags) {
  $opportunity = (object) array();
  _rules_insightly_create_default_fields($fields, $opportunity);
  _rules_insightly_create_custom_fields($custom_fields, $opportunity);
  _rules_insightly_create_tags($tags, $opportunity);
  $opportunity->OPPORTUNITY_STATE = "OPEN";
  $args = func_get_args();
  drupal_alter('rules_insightly_action_add_update_opportunity', $opportunity, $args);
  $i = rules_insightly_get_object();
  try {
    $add = $i->addOpportunity($opportunity);
  } catch (Exception $e) {
    watchdog('rules_insightly_action_add_update_opportunity', $e->getMessage(), WATCHDOG_ERROR);
  }
  return $add;
}

/**
 * Action callback: Add organization on insightly
 */
function rules_insightly_action_add_organization($fields, $custom_fields, $contactinfos, $addresses, $tags) {
  $organization = (object) array();
  _rules_insightly_create_default_fields($fields, $organization);
  _rules_insightly_create_custom_fields($custom_fields, $organization);
  _rules_insightly_create_contact_info($contactinfos, $organization);
  _rules_insightly_create_addresses($addresses, $organization);
  _rules_insightly_create_tags($tags, $organization);
  $args = func_get_args();
  drupal_alter('rules_insightly_action_add_organization', $organization, $args);
  $i = rules_insightly_get_object();
  try {
    $add = $i->addOrganization($organization);
  } catch (Exception $e) {
    watchdog('rules_insightly_action_add_organization', $e->getMessage(), WATCHDOG_ERROR);
  }
  return $add;
}

/**
 * Action callback: Update organization on insightly
 */
function rules_insightly_action_update_organization($organization_name,$fields, $custom_fields, $contactinfos,$addresses, $tags) {
  $organization = _rules_insightly_get_organization_by_name($organization_name);
  _rules_insightly_create_default_fields($fields, $organization);
  _rules_insightly_create_custom_fields($custom_fields, $organization);
  _rules_insightly_create_contact_info($contactinfos, $organization);
  _rules_insightly_create_addresses($addresses, $organization);
  _rules_insightly_create_tags($tags, $organization);
  $args = func_get_args();
  drupal_alter('rules_insightly_action_update_organization', $organization, $args);
  try {
    $update = $i->addOrganization($organization);
  } catch (Exception $e) {
    watchdog('rules_insightly_action_update_organization', $e->getMessage(), WATCHDOG_ERROR);
  }
  return $update;
}
/**
 * Convert the values provided as a string in the action fields 
 * in the format that's specified in the description into an array 
 * containing the field name as a key with its value 
 */
function _rules_insightly_prepare_fields($values,$field_delimiter = ";",$value_delimiter = "|") {
  $return = array();
  if (empty($values)) {
    return;
  }
  $fields = explode($field_delimiter, $values);
  foreach ($fields as $value) {
    if ($value) {
      $field = explode($value_delimiter, $value);
      $return[$field[0]] = $field[1];
    }
  }
  return $return;
}

/**
 * Loads an opportunity from insightly by its name
 */
function _rules_insightly_get_opportunity_by_name($opportunity_name) {
  $i = rules_insightly_get_object(); 
  $opportunities = $i->getOpportunities();
  foreach ($opportunities as $key => $opportunity) {
    if ($opportunity->OPPORTUNITY_NAME == $opportunity_name) {
      return $opportunity;
    }
  }
  return (object) array();
}

/**
 * Loads an organization from insightly by its name
 */
function _rules_insightly_get_organization_by_name($organization_name) {
  $i = rules_insightly_get_object(); 
  $organizations = $i->getOpportunities();
  foreach ($organizations as $key => $organization) {
    if ($organization->ORGANISATION_NAME == $organization_name) {
      return $organization;
    }
  }
  return (object) array();
}
/**
 *  Create Tag Object
 */
function _rules_insightly_tag($value) {
  $tag = (object) array();
  $tag->TAG_NAME = $value;
  return $tag;
}

/**
 * Creates custom field object
 */
function _rules_insightly_custom_field($field_id, $value) {
  $custom_field = (object) array();
  $custom_field->CUSTOM_FIELD_ID = $field_id;
  $custom_field->FIELD_VALUE = $value;
  return $custom_field;
}

/**
 * Creates contact info object of type email 
 */
function _rules_insightly_contact_email($email) {
  $contact_email = (object) array();
  $contact_email->TYPE = 'EMAIL';
  $contact_email->LABEL = 'Email';
  $contact_email->DETAIL = $email;
  return $contact_email;
}

/**
 * Creates contact info object
 */
function _rules_insigthly_contact_info($type, $value) {
  $contact_info = (object) array();
  $contact_info->TYPE = $type;
  $contact_info->DETAIL = $value;
  return $contact_info;
}

/**
 * Creates contact address object
 */
function _rules_insigthly_address($type, $address_values) {
  $address = (object) array();
  $address->ADDRESS_TYPE = $type;
  foreach ($address_values as $key => $value){
    $field_key = trim($key);
    $address->$field_key = $value;
  }
  return $address;
}

function _rules_insightly_prepare_address($address_values){
  $add_val = trim($address_values,"{}");
  $values = _rules_insightly_prepare_fields($add_val,",","::");
  return $values;
}
/**
 * Checks if an email contact info exist in an insightly contact
 */
function _rules_insightly_contact_email_exist($email, $contact) {
  $contactinfos = $contact->CONTACTINFOS;
  foreach ($contactinfos as $info) {
    if ($info->DETAIL == $email) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Checks if an email contact exist in an insightly contact.
 */
function _rules_insightly_contact_info_exist($value, $contact) {
  $contactinfos = $contact->CONTACTINFOS;
  foreach ($contactinfos as $info) {
    if ($info->DETAIL == $value) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Checks if an email contact exist in an insightly contact.
 */
function _rules_insightly_address_exist($value, $entity) {
  $addresses = $entity->ADDRESSES;
  foreach ($addresses as $address) {
    if ($address == $value) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Checks if an email contact exist in an insightly entity.
 */
function _rules_insightly_custom_field_exist($field_id, $entity, $return_key = FALSE) {
  if(!isset($entity->CUSTOMFIELDS)){
    return FALSE;
  }
  $custom_fields = $entity->CUSTOMFIELDS;
  foreach ($custom_fields as $key => $custom_field) {
    if ($custom_field->CUSTOM_FIELD_ID == $field_id) {
      if ($return_key) {
        return $key;
      }
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Checks if tag exists in an insightly entity.
 */
function _rules_insightly_tag_exists($value, $entity) {
  $tags = $entity->TAGS;
  foreach ($tags as $tag) {
    if ($tag->TAG_NAME == $value) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Set fields values for an insightly entity
 */
function _rules_insightly_create_default_fields($fields, &$entity) {
  $entity_fields = _rules_insightly_prepare_fields($fields);
  if(!empty($entity_fields)){
    foreach ($entity_fields as $key => $value) {
      $field_key = trim($key);
      $entity->$field_key = $value;
    }
  }
}

/**
 * Set contact info for an insightly contact
 */
function _rules_insightly_create_contact_info($contactinfos, &$contact) {
  $contact_infos = _rules_insightly_prepare_fields($contactinfos);
  if(!empty($contact_infos)){
    foreach ($contact_infos as $key => $value) {
      $field_key = trim($key);
      $customfield = _rules_insigthly_contact_info($field_key, $value);
      $contact->CONTACTINFOS[] = $customfield;
    }
  }
}

/**
 * Set contact info for an insightly entity
 */
function _rules_insightly_create_addresses($addrs, &$entity) {
  $addresses = _rules_insightly_prepare_fields($addrs);

  if(!empty($addresses)){
    foreach ($addresses as $key => $value) {
      $field_key = trim($key);
      $address_values =  _rules_insightly_prepare_address($value);
      $address = _rules_insigthly_address($field_key, $address_values);
      $entity->ADDRESSES[] = $address;
    }
  }
}

/**
 * Set custom fields for an insightly entity
 */
function _rules_insightly_create_custom_fields($custom_fields, &$entity) {
  $entity_custom_fields = _rules_insightly_prepare_fields($custom_fields);
  if (!empty($entity_custom_fields)) {
    foreach ($entity_custom_fields as $key => $value) {
      if (!empty($value)) {
        $field_key = trim($key);
        $customfield = _rules_insightly_custom_field($field_key, $value);
        if (_rules_insightly_custom_field_exist($field_key, $entity)) {
          $key = _rules_insightly_custom_field_exist($field_key, $entity, TRUE);
          $entity->CUSTOMFIELDS[$key] = $customfield;
        } else {
          $entity->CUSTOMFIELDS[] = $customfield;
        }
      }
    }
  }
}

/**
 * Set tags for an insightly entity
 */
function _rules_insightly_create_tags($tags, &$entity) {
  $entity_tags = _rules_insightly_prepare_fields($tags);
  if(!empty($entity_tags)){
    foreach ($entity_tags as $key => $value) {
      if (!_rules_insightly_tag_exists($value, $entity)) {
        $tag = _rules_insightly_tag($value);
        $entity->TAGS[] = $tag;
      }
    }
  }
}
